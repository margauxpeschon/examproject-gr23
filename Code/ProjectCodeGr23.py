#code for the part 2
from math import isnan
from random import *

class coffeeshop(object):
    def __init__(self, customerList, orderList, menuItemList):
        """
        :param customerList: the list of all customers who buys something in the coffeeshop, Customer[]
        :param orderList: the list of all orders made in the coffeeshop, order[]
        :param menuItemList: all products that can be purchased in the coffeeshop, menuItem[]
        """
        self.customerList = customerList
        self.orderList = orderList
        self.menuItemList = menuItemList

    def income(self, startPeriod, endPeriod):
        """
        :param startPeriod: starting date of the period we want to get the income of, Timestamp
        :param endPeriod: ending date of the period we want to get the income of, Timestamp
        :return: income, integer
        """
        income = 0
        for elt in self.orderList:
            #selecting the right period to calculate the average.
            if elt.timePurchase >= startPeriod and elt.timePurchase <= endPeriod :
                income += elt.costOrder
        return income


class client(object):
    def __init__(self, customerID, type):
        """
        :param customerID: The ID of the client, string
        :param type: The type of the client (one-time customers, customers from Tripadvisor, returning customer and hipster, string
        """
        #Initialize budget
        if type == 'regularOneTime' or type == 'tripadvisor':
            budget = 100
        elif type == 'regularReturning':
            budget = 250
        elif type == 'hipster':
            budget = 500
        else :
            budget = 0
        self.customerID = customerID                             # DU CHANHER L ATTRIBU CAR LE MEME DANS ORDER ET CA POSE PROBLEME
        self.type = type
        self.budget = budget
        #PS : all the clients have a history even the one time customers
        self.history = dict()

    def makeAnOrder(self, Date, menuItemList, Probabilities):
        """
        :param Date: the date and time at which the customer makes an order, Timestamp
        :param menuItemList: List of all the object-item in the menu, menuItem[]
        :param Probabilities: The probabilities of purchase, DataFrame
        :return: the object of the order made, Order
        """
        #specify which food and drinks are the most likely to be purchased at this time
        drink_purchased, food_purchased = menuItem.giveMostLikelyItems(Date, menuItemList, Probabilities)
        if food_purchased != 0 and drink_purchased != 0:
            #create the order
            order_made = order(Date, self.customerID, drink_purchased, food_purchased)
        elif drink_purchased != 0 and food_purchased == 0:
            #create the order with only a drink purchased
            order_made = order(Date, self.customerID, drink_purchased)

        #we modify the budget
        order_made.modifyBudget(self)
        #Adding the order in the history of the customer
        self.modifyHistory(order_made)
        return order_made

    def modifyHistory (self, current_order) :
        """
        :param current_order: the object of the order that is currently taking place, order
        """
        self.history[current_order.timePurchase] = [current_order.drinkPurchased, current_order.foodPurchased, self.budget]

    def getHistory (self) :
        """
        :return: the history (what drink was purchased, what food was purchased and the budget of this client at this time) of the client, dict()
        """
        print('Client : %s  ***** Order history ' % (self.customerID))
        if len(self.history) != 0 :
            for date in self.history :
                print ('*****%s*****\n'%(date)  )
                #what drinks was purchased
                drink = self.history[date][0].name
                if self.history[date][1] != 0 :
                    #what food was purchased
                    food = self.history[date][1].name
                else : food = '0'
                #what was the budget of the customer at this moment
                budget = self.history[date][2]
                print ('drink ordered : %s  \nfood ordered : %s \nbudget : %s \n' %(drink, food, budget))
        else :
            print('This customer still not came in the coffeeshop at this moment :( ')


class tripadvisorClient(client):
    def getTip(self):
        """
        :return: a random numer between 1 and 10 which represent the tip of the client
        """
        return(randint(1,10))


class order(object):
    def __init__(self, timePurchase, customerId, drinkPurchased, foodPurchased =0):
        """
        :param timePurchase: the moment (hours, minutes, second) where the order was made, timestanmp
        :param customerId: the ID of the customer who made this order, object
        :param drinkPurchased: the drink purchased in the order, object
        :param foodPurchased: optional parameter, if no food is encoded then the assume value of the parameter is, object
        """
        self.timePurchase = timePurchase
        self.customerId = customerId
        self.foodPurchased = foodPurchased
        self.drinkPurchased = drinkPurchased
        self.costOrder = 0

    def getPriceOrder(self, customer):
        """
        :param : the customer object
        :return: the calculated price of the order, including the price of the drink and the potential food
        """
        priceOrder = self.drinkPurchased.price
        if self.foodPurchased != 0:
            priceOrder += self.foodPurchased.price
        if customer.type == 'tripadvisor':
            priceOrder += tripadvisorClient(customer.customerID, customer.type).getTip()                          # CHANGE THE TIP IN PRICE ORDER TO GET IT IN AVRG
        self.costOrder = priceOrder
        return priceOrder

    def modifyBudget(self, customer):
        """
        :param customer: the customer we want to modfy the budget
        """
        customer.budget -= self.getPriceOrder(customer)



class menuItem(object):
    def __init__(self, name, price):
        self.name = name
        self.price = price

    def giveMostLikelyItems(date, menuItemList, Probabilities):
        """
        :param date: the moment we want to know what is the item wwith the highest probability to be purchased.
        :param menuItemList: List of all the object-items in the menu, object[]
        :param Probabilities: Dataframe of the probabilities of purchase, DataFrame
        :return: the object of the drink and food the most likely to be purchased at this time, object
        """
        actualHour = date[11:]
        #get the name of the column with the highest probability
        Probabilites_food = Probabilities[Probabilities.columns[12:16]].loc[actualHour].idxmax(axis=0, skipna=None)
        Probabilities_drinks = Probabilities[Probabilities.columns[16:]].loc[actualHour].idxmax(axis=0)

        #check if there is a food ordered at this hour (not only nan)
        if not isnan(Probabilities[Probabilities.columns[12:16]].loc[actualHour].max(axis=0)):
            #the name of the column = Proba+food name so we only take the food name
            foodPurchased = Probabilites_food[5:]
        else:
            foodPurchased = 0

        drinkPurchased = Probabilities_drinks[5:]
        # get the object which corresponds to this food name
        for index in range(len(menuItemList)):
            if menuItemList[index].name == foodPurchased:
                foodPurchased = menuItemList[index]
            if menuItemList[index].name == drinkPurchased:
                drinkPurchased = menuItemList[index]
        return drinkPurchased,foodPurchased

