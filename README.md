# README #

This README  documents whatever steps are necessary to get the project running

### What is this repository for? ###

* this repository includes the code to analyze the coffee_shop dataframe and to simulate 5 other years of this coffeeshop

### How do I get set up? ###

* in this repository you have 3 folders : Data, Code and Simulation
* Fist you need to copy the exel file Coffeebar_2013-2017 in the Data folder. Then you can run the code of the Exploratory-Gr23 file which is in the same folder.
* at the end of the running you will have a new file in the Datafolder which is Probabilities. Once you have this folder you can continue
* then you can run the simulation. at the end of the simmulation dataframe is created which you will need to show the plots. 
* to create the plots you only need to execute them except for the second one. The change the day of the graph you need to follow the instructions in the code'comment.
* The part 4 is also in the Project-Simulation-Gr23 file. 
* always run the code in the console (alt+maj+e)

### Who do I talk to? ###

* Repository of the Group 23 including Lazzaro Leana and Peschon Margaux.