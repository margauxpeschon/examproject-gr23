from Code.ProjectCodeGr23 import *
import os
import matplotlib.pyplot as plt
path = os.path.abspath('.')
import pandas as pd
import numpy as np

path = os.path.abspath('.\Data')
#import the excel documents of the coffeeshop and the probabilities
Probabilities_wrong_index =	pd.read_csv(path + '/Probabilities.csv')
Probabilities = Probabilities_wrong_index.set_index('HOURS')
coffee_shop = pd.read_csv(path + '/Coffeebar_2013-2017.csv', sep=";")


#***************
#**Simulation***
#***************

# creation of the items of the menu
menu_item_list = []

for item in (Probabilities.columns.tolist()[0:4] + Probabilities.columns.tolist()[4:10]):
    if item == 'sandwich' or item == 'milkshake':
        price = 5
        max_price = 2 * price
    elif item == 'cookie' or item == 'water':
        price = 2
    elif item == 'frappucino':
        price = 4
    else:
        price = 3
    menu_item_list.append(menuItem(item, price))


# creation of the clients 1000 returning clients:
# the ID of the returning customers looks like this 'CID0000xxxx' where xxxx is a nbr between 0 et 1000
customer_list = []
nbr_returnings = 1000
for returningClients in range(nbr_returnings):
    IDs = 'CID' + str(returningClients).zfill(8)
    if returningClients <= nbr_returnings*2/3:
        customer_list.append(client(IDs, 'regularReturning'))
    else:
        customer_list.append(client(IDs, 'hipster'))


#** LOOP INITIALIZATION **
# we create a list containing the last 4 numbers of the IDs of the available returning customer
available_nbrID_returning = [elt for elt in range(nbr_returnings)]
# starting point of the IDs of the unique customers
nbr_non_returning = nbr_returnings
order_list = []
#Putting all the different dates (TIME) from the coffeeshop in a list
list_time_coffeeshop = coffee_shop['TIME'].values

#** 5 YEARS SIMULATION LOOP **
#Creation of the orders at each period of time during 5 years
for iter, date in enumerate(list_time_coffeeshop) :

    # if we are in the scenario where the client is a one time customer or if there is no returning customer with enough budget !
    if randint(1, 10) > 2 or len(available_nbrID_returning) == 0:
        #creation of a new idea for this new customer
        ID = 'CID' + str(nbr_non_returning).zfill(8)

        # one time out of 10 the on time client is a tripadvisor client
        if randint(1, 10) <= 1:
            customer_list.append(client(ID, 'tripadvisor'))
        else:
            customer_list.append(client(ID, 'regularOneTime'))

        nbr_non_returning += 1
        current_client = customer_list[-1]

    # if the client is a returning client
    else:
        #choosing a number in the available returning IDs' 4 last number
        random_nbr = choice(available_nbrID_returning)
        #getting the client in the client list using the 4 last numbers of the ID to determine its position.
        current_client = customer_list[random_nbr]


    # the client makes an order :
    current_order = current_client.makeAnOrder(date, menu_item_list, Probabilities)
    order_list.append(current_order)

    # checking if the customer is a returning customer out of budget :
    if current_client.type == 'hipster' or current_client.type == 'regularReturning':
        if current_client.budget < max_price:
    #dropping the ID of the no longer available returning customer out of the available IDs of the retruning customers.
            available_nbrID_returning.remove(int(current_client.customerID[3:]))

# ****************************
# *creation of the coffeeshop*
# ****************************

café = coffeeshop(customerList=customer_list, orderList=order_list, menuItemList=menu_item_list)

# ****************************
# *creation of the Dataframe**
# ****************************

list_ID =[]
list_drinks = []
list_foods = []
list_time = coffee_shop['TIME'].values
for index in range (len(order_list)) :
    list_ID.append(order_list[index].customerId)
    list_drinks.append(order_list[index].drinkPurchased.name)
    if order_list[index].foodPurchased == 0 :
    #we fill in the list with a "nan" when there is no food ordered
        list_foods.append(np.nan)
    else :
        list_foods.append(order_list[index].foodPurchased.name)


Simulation = pd.DataFrame({'TIME':list_time,
                           'CUSTOMER': list_ID,
                           'DRINKS' : list_drinks,
                           'FOOD':list_foods})





#----------------
#    PLOTS
#*---------------

#get the amount of days in the 5 years simulation :
def getdays (x) :
    return x[:10]
coffee_shop['DAYS'] = coffee_shop['TIME'].map(getdays)
#Get the number of orders (customers) in a day
Count_customers = coffee_shop[['CUSTOMER', 'DAYS']].groupby('DAYS').count()      #there is always the same amount of customers (orders made) during a day
#number of order per day
nbrOrderPerDay = int(Count_customers['CUSTOMER'].mean())


#plot of daily income in the coffeeshop
list_days = coffee_shop.drop_duplicates(subset = 'DAYS')['DAYS'].values
averages = []
for day in list_days :
    start_period = day + ' 08:00:00'
    end_period = day + ' 17:59:00'
    #get the income for the day
    average = café.income(start_period,end_period)
    averages.append(average)

plt.xlabel('days')
plt.ylabel('daily income')
plt.plot(averages, 'ro')
plt.show()

#growth of the income during a specific day (cumulative income per hour)
accumulated_income_day = []
hours = []
total = 0
start_period = 0            #if you want a specific day = nbrOrderPerDay * day_you_want + 1
for day_hour in range (0,(nbrOrderPerDay-1)):
    end_period = start_period +day_hour
    #get the income for the day
    average = café.income(Simulation['TIME'][start_period],Simulation['TIME'][end_period])
    accumulated_income_day.append(average)
    hours.append(str(Simulation['TIME'][day_hour])[11:13])
plt.xlabel('hours')
plt.ylabel('cumulated income')
plt.plot(hours,accumulated_income_day)
plt.show()

#food and drink sold per year
list_year = ['2013', '2014', '2015', '2016', '2017']
consumptionSimul = pd.DataFrame(0,index = list_year ,columns = [elt.name for elt in menu_item_list])
for iter, date in Simulation['TIME'].iteritems () :
    year = date[0:4]
    food_bought = Simulation['FOOD'][iter]
    drink_bought = Simulation['DRINKS'][iter]
    consumptionSimul.loc[year, drink_bought] += 1
    if pd.notna(food_bought):
        consumptionSimul.loc[year, food_bought]+= 1
consumptionSimul['TOTAL FOOD'] = consumptionSimul.iloc[:, 0:4].sum(axis = 1)
consumptionSimul['TOTAL DRINKS'] = consumptionSimul.iloc[:, 4:10].sum(axis = 1)

plt.xlabel('Years')
consumptionSimul[[elt.name for elt in menu_item_list[:4]]].plot.bar()
plt.ylabel('Total amount of food sold')

consumptionSimul[[elt.name for elt in menu_item_list[4:]]].plot.bar()
plt.ylabel('Total amount drinks sold')

consumptionSimul[['TOTAL FOOD','TOTAL DRINKS']].plot.bar()
plt.ylabel('Total amount of food and drinks sold')

consumptionSimul.plot.bar()
plt.ylabel('total sells of food and drinks')


#proportion of order made by returning and made by one-time :
ClientsOnetime = Simulation.drop_duplicates(subset = 'CUSTOMER', keep = False)['CUSTOMER'].count()
ClientRet = Simulation[Simulation.duplicated(['CUSTOMER'], keep=False)]['CUSTOMER'].count()

labels = 'One Time Customer', 'Returning Customer'
sizes = [ClientsOnetime, ClientRet]
colors = ['gold', 'lightskyblue']
plt.xlabel('PROPORTION OF ORDER MADE BY ')
plt.ylabel('')
plt.pie(sizes, labels=labels, colors=colors, autopct='%1.1f%%', startangle=0)
plt.show()





#**************
#***part 4*****
#**************


#get the history of a returning client
client = choice(café.customerList[0:1000])
client.getHistory()


#divide the dataframe into a returning client dataframe and one time customer dataframe.
def getHour (x) :
    return x[11:]
coffee_shop['HOURS'] = coffee_shop['TIME'].map(getHour)
ClientsOnetime = coffee_shop.drop_duplicates(subset = 'CUSTOMER', keep = False)
ClientRet = coffee_shop[coffee_shop.duplicated(['CUSTOMER'], keep=False)]

#amount of returning customers :
print(len(ClientRet.drop_duplicates(subset='CUSTOMER')))

#Find the most frequent hours at which regular customers show up
visitHourFreqReturning = ClientRet['HOURS'].value_counts ()  #increasing order

#we take for instance the top 20 of the most visited hours
liste = visitHourFreqReturning.index[0:20].tolist()
for hours in liste :
    print(hours)



#probability of having a returning at each time:
customer_count = coffee_shop[['CUSTOMER', 'HOURS']].groupby('HOURS').count()
customer_count_ret = ClientRet[['CUSTOMER', 'HOURS']].groupby('HOURS').count()
customer_count_ot = ClientsOnetime[['CUSTOMER', 'HOURS']].groupby('HOURS').count()
counts = [ customer_count,  customer_count_ret, customer_count_ot ]
#https://asciinema.org/a/106755
prob = pd.concat(counts, axis=1, keys=['customer_count', 'ret_count', 'ot_count'])

prob['percentRet'] = prob['ret_count'] / prob['customer_count']

#food and drinks probabilities per hour for regulars and onetime
prob['Food_tot'] = coffee_shop[['FOOD', 'HOURS']].groupby('HOURS').count()
prob['Drink_tot'] = coffee_shop[['DRINKS', 'HOURS']].groupby('HOURS').count()
prob['Food_ret'] = ClientRet[['FOOD', 'HOURS']].groupby('HOURS').count()
prob['Drink_ret'] = ClientRet[['DRINKS', 'HOURS']].groupby('HOURS').count()
prob['Food_ot'] = ClientsOnetime[['FOOD', 'HOURS']].groupby('HOURS').count()
prob['Drink_ot'] = ClientsOnetime[['DRINKS', 'HOURS']].groupby('HOURS').count()

# example : the probability that a drink is bought by a returning customer at 17h56 is of 0.8%
prob['Food_ret_prob'] = prob['Food_ret'] / prob['Food_ret'].sum()
prob['Drink_ret_prob'] = prob['Drink_ret'] / prob['Drink_ret'].sum()
prob['Food_ot_prob'] = prob['Food_ot'] / prob['Food_ot'].sum()
prob['Drink_ot_prob'] = prob['Drink_ot'] / prob['Drink_ot'].sum()

