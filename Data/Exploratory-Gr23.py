import sys
sys._enablelegacywindowsfsencoding()
import pandas as pd
import os
import matplotlib.pyplot as plt

#get the data from Coffeebar that we put in a folder called 'Data'
path = os.path.abspath('.\Data')
coffee_shop = pd.read_csv(path + '/Coffeebar_2013-2017.csv', sep=";")

#drink sold by the coffee bar
list_drinks_served  = coffee_shop.drop_duplicates(subset = 'DRINKS')['DRINKS'].tolist()

#food sold by the cofee bar
list_food_served = coffee_shop.drop_duplicates(subset = 'FOOD').dropna()['FOOD'].tolist()

#how many unique customer did the coffeshop have ?
ClientsOnetime = coffee_shop.drop_duplicates(subset = 'CUSTOMER', keep = False)['CUSTOMER'].count()

#create a dictionnary including each item of the menu as a key and as value a liste filled with 1 when the order included this item zero if not
dico_item = dict()
for element in list_food_served + list_drinks_served:
    dico_item[element] = []

#fill in the dictionnary
for iter in range (len(coffee_shop)) :
    drink_bought = coffee_shop['DRINKS'][iter]
    food_bought = coffee_shop['FOOD'][iter]
    for key in dico_item :
        if key == drink_bought :
            dico_item[key].append(1)
        elif key == food_bought :
            dico_item[key].append(1)
        else :
            dico_item[key].append(0)

#put the dico into a dataframe
sellings = pd.DataFrame(dico_item,index= coffee_shop['TIME'])
#reorder the dataframe into a non alphabetical one but a dataframe ordered as the list_food_served and list_drink_served
sellings = sellings[ list_food_served + list_drinks_served]



#getting the annual sales

def getYear (x) :
    """
    :param x: the time (timestamp)
    :return: the year of x (timestamp)
    """
    return x[0:4]
#Adding a new columns in our Dataframe with the years where the order was made
sellings['YEAR'] = sellings.index.map(getYear)
#creating a new dataframe of the number of foods and drinks sold for each years
consumption = sellings.groupby('YEAR').sum()
#Adding 2 columns of total foods and total drinks purchased per year
consumption['totalFoods'] = consumption[consumption.columns[0:4]].sum(axis = 1)
consumption['totalDrinks'] = consumption[consumption.columns[4:10]].sum(axis = 1)

#---------
# PLOTS
#---------

#plot of drink sell by years
consumption[list_food_served].plot.bar()
plt.xlabel('Years')
plt.ylabel('Total amount of food sold')
#plot of foods sold by years
consumption[list_drinks_served].plot.bar()
plt.ylabel('Total amount drinks sold')
#plot of foods and drinks sold by years
consumption[['TOTAL FOOD','TOTAL DRINKS']].plot.bar()
plt.ylabel('Total amount of all food and drinks sold')
#plot of all shown before
consumption.plot.bar()
plt.ylabel('total sells of food and drinks')

#proportion of order made by returning or one time customers
ClientRet = coffee_shop[coffee_shop.duplicated(['CUSTOMER'], keep=False)]['CUSTOMER'].count()
labels = 'One Time Customer', 'Returning Customer'
sizes = [ClientsOnetime, ClientRet]
colors = ['yellowgreen','lightcoral']
# Plot of proportion of order made by returning or one time customers
plt.xlabel('proportion of order made by ...')
plt.pie(sizes, labels=labels, colors=colors, autopct='%1.1f%%', startangle=0)
plt.show()



#Create the Probabilities Dataframe

def getHour (x) :
    """
    :param x: the time (timestamp)
    :return: the hours, minutes and seconds of x (timestamp)
    """
    return x[11:]

#Adding in the previous DataFrame a column with the moment (hours, minutes, seconds)
sellings['HOURS'] = sellings.index.map(getHour)
#Creating a new DataFrame with the number of foods and drinks ordered at any given time (hours, minutes and seconds)
Probabilities = sellings.groupby('HOURS').sum()

#Creating 2 columns of total foods and total drinks purchased at any given time (hours, minutes seconds)
Probabilities['totalFoods'] = Probabilities[Probabilities.columns[0:4]].sum(axis = 1)
Probabilities['totalDrinks'] = Probabilities[Probabilities.columns[4:10]].sum(axis = 1)
#creating each probabilities
for element in list_food_served :
    Probabilities['Proba'+ element] = Probabilities[element] / Probabilities.totalFoods
for element in list_drinks_served :
    Probabilities['Proba'+ element] = Probabilities[element] / Probabilities.totalDrinks


#Saving the probabilities Dataframe into a csv file
Probabilities.to_csv('Probabilities.csv',	encoding='utf-8')


